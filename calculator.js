var cls_dropdown = document.getElementById('class_dropdown');

var req = new XMLHttpRequest();
req.open('GET', '/classes.json');
req.onreadystatechange = function() {
    if (this.readyState == 4) {
        var classes = JSON.parse(this.responseText);

        classes.forEach(function(cls) {
            var option = document.createElement('option');
            option.text = cls.name;
            cls_dropdown.add(option);
        });

        var skill_list = document.getElementById('skill_list');

        function on_class_change(e) {
            var cls = e.target.value;
            if (cls === '<Choose>') {
                while (skill_list.firstChild) {
                    skill_list.removeChild(skill_list.lastChild);
                }
            } else {
                cls_obj = classes.filter(function(obj) {
                   return obj.name === cls;
                })[0];
                var skills = cls_obj.skills;

                skills.forEach(function(skill) {
                    var li = document.createElement('li');
                    li.innerHTML = skill;
                    skill_list.appendChild(li);
                });
            }
        }

        //var skill_list = document.getElementById('skill_list');
        cls_dropdown.addEventListener('change', on_class_change);
    }
}
req.send();
